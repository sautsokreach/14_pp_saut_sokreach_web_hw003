tailwind.config = {
    theme: {
      extend: {
        colors: {
          red_light : "rgba(220, 20, 60, 0.24)"
        },
        width:{            
        },
        height:{
        },
        fontFamily: {
        },
        screens: {
        },
        backgroundImage: {
          'page': "url('img/bg.jpg')",          
        },
        lineHeight: {
        },
      },
    },
  };
  