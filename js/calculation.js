    let index=0;
    let startTime = new Date();
    let endTime = new Date();



    const calculate = ()=>{
        const text_calculate = [["bg-red-500","Stop","stop.png"],["bg-amber-300","Clear","closed.png"],[" bg-green-600","Start","play-button.png"]];
        const action = document.getElementById("action");
        action.className=` ${text_calculate[index%3][0]} h-12 w-2/6  font-bold text-black flex justify-center items-center rounded-xl`;
        action.innerHTML = `<img src="img/${text_calculate[index%3][2]}" class="h-10 inline-block"> &nbsp`+ text_calculate[index%3][1];
        event_action[index%3]();
        index++;
    }
    const event_action = [
        function start() {
        startTime = new Date( Date.now());
        document.getElementById("start_time_txt").innerHTML= startTime.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
        },
        function stop(){
        endTime = new Date( Date.now());
        let minute = ((endTime-startTime)/1000/60).toFixed(0);
        let money = price(minute)  ;
        document.getElementById("stop_time_txt").innerHTML= endTime.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'}) ;
        document.getElementById("minute").innerHTML=  minute;
        document.getElementById("money").innerHTML=  money;
        },
        function clear(){
        document.getElementById("start_time_txt").innerHTML= "" ;
        document.getElementById("stop_time_txt").innerHTML= "" ;
        document.getElementById("minute").innerHTML= "" ;
        document.getElementById("money").innerHTML= "" ;
        }
    ];

    const price = (minute) => {
        var left;
        if(minute == 0 ) left= 500;
        else if(minute%60 == 0 ) left= 0;
        else if(minute%60 < 16) left= 500;
        else if(minute%60 < 31) left= 1000;
        else left= 1500;
        return left+(parseInt(minute/60)*1500);
    }
    
    let realtime =()=>{
        document.getElementById("realTime").innerHTML= '<img src="img/clock.png" class="h-10 inline-block">&nbsp;' + new Date( Date.now()).toDateString()+ "&nbsp;" +new Date( Date.now()).toLocaleTimeString();
    }
    realtime()
    setInterval(function(){
        realtime()
    },1000);
   

    